# -*- coding: utf-8 -*-

################################################################################
#
#      DDL for Call Report Tables
#       this was hand crafted to match as closely possible the
#       name and structure of the Crowe tables
#
################################################################################

"""
Created on 2015.04.23

@author: pjc
@version: 0.1
"""

def DDL_Branch():
    ddl = """
    create table if not exists Branches(
        Branch_ID int NOT NULL,
        RSSD_ID int NOT NULL,
        Bank_Name nvarchar(100) NOT NULL,
        Branch_Name nvarchar(100) NOT NULL,
        Certificate_Number int NOT NULL,
        Main_Office_Flag tinyint NOT NULL,
        Address nvarchar(80),
        City nvarchar(60) NOT NULL,
        State_Code char(2) NOT NULL,
        State nvarchar(35) NOT NULL,
        Zip char(5) NOT NULL,
        County nvarchar(50),
        Office_Number smallint,
        FDIC_Bank_Number int,
        Bank_Class nvarchar(2),
        CBSA nvarchar(60),
        CBSA_Divisions_Name nvarchar(60),
        CBSA_Divisions_Number int,
        CBSA_Metro_Flag tinyint,
        CBSA_Metro_Code int,
        CBSA_Metro_Name nvarchar(60),
        CBSA_Micro_Flag tinyint,
        CBSA_Number int,
        CSA nvarchar(60),
        CSA_Flag tinyint,
        CSA_Number smallint,
        Established_Date date,
        Service_Type_Code tinyint,
        State_and_County_Number int,
        Run_Date date NOT NULL,
        Last_Update_Date smalldatetime
        )
    """
    return ddl

def DDL_CallReportData():
    ddl = """
        CREATE TABLE CallReportData(
            RSSD_ID int NOT NULL,
            Call_Report_Type nvarchar(5) NOT NULL,
            Reporting_Period date NOT NULL,
            MDRM_Code nvarchar(15) NOT NULL,
            MDRM_Value numeric(18, 6) NULL,
            MDRM_Text nvarchar(3000) NULL,
            Last_Submission_Date datetime NOT NULL,
            Load_Date datetime NOT NULL
          )
        """
    return ddl

def DDL_Demographics():
    ddl = """
      CREATE TABLE Demographics(
        RSSD_ID int NOT NULL,
        Bank_Name nvarchar(100),
        Address nvarchar(80),
        City nvarchar(60) NOT NULL,
        State_Code char(2) NOT NULL,
        State nvarchar(35),
        Zip char(5) NOT NULL,
        County nvarchar(50),
        ABA_Routing_Number int,
        Certificate_Number int,
        Docket_Number int,
        Parent_RSSD_ID int,
        Holding_Company_Name nvarchar(100),
        Charter_Class nvarchar(2),
        Number_of_US_Offices smallint,
        Number_of_Foreign_Offices smallint,
        Interstate_Brances tinyint,
        Established_Date date,
        Asset_Concentration_Hierarchy tinyint,
        Subchapter_S_Corp tinyint,
        CBSA_Metro_Code char(5),
        CBSA_Metro Name nvarchar(60),
        Deposit_Insurance_Date date,
        Directly_Owned_by_Another_Bank int,
        Trust_Powers smallint,
        Regulator nvarchar(5),
        Insurance_Fund_Membership nvarchar(5),
        FDIC_Geographic_Region nvarchar(30),
        FDIC_Supervisory_Region nvarchar(30),
        FDIC_Field_Office nvarchar(30),
        Federal_Reserve_District nvarchar(30),
        Comptroller_District_Office nvarchar(255),
        BankruptcyIndicator nvarchar(20),
        NonThrift_Supervision_Region_Office nvarchar(20),
        Web_Address nvarchar(130),
        Effective_Date date NOT NULL,
        PeriodTo date,
        Report_Date date NOT NULL,
        Run_Date date NOT NULL,
        PeriodFrom date NOT NULL,
        Last_Update Date smalldatetime NOT NULL,
        Holding_Company_Flag nvarchar(3)
      )
    """
    return ddl

def DDL_FinancialStatement():
    ddl = """
      CREATE TABLE FinancialStatement(
        RSSD_ID int NOT NULL,
        Call_Report_Type nvarchar(5) NOT NULL,
        Reporting_Period date NOT NULL,
        item01_First_lien_mortgages numeric(18, 0),
        item01_First_lien_mortgages_net_chargeoffs numeric(18, 0),
        item02_Closed_end_junior_liens numeric(18, 0),
        item02_Closed_end_junior_liens_net_chargeoffs numeric(18, 0),
        item03_HELOCs numeric(18, 0),
        item03_HELOCs_net_chargeoffs numeric(18, 0),
        item04_CnI_Loans numeric(18, 0),
        item04_CnI_Loans_net_chargeoffs numeric(18, 0),
        item05_1_4_family_construction_loans numeric(18, 0),
        item05_1_4_family_construction_loans_net_chargeoffs numeric(18, 0),
        item06_Other_construction_loans numeric(18, 0),
        item06_Other_construction_loans_net_chargeoffs numeric(18, 0),
        item07_Multifamily_loans numeric(18, 0),
        item07_Multifamily_loans_net_chargeoffs numeric(18, 0),
        item08_Non_farm_non_residential_owner_occupied_loans numeric(18, 0),
        item08_Non_farm_non_residential_owner_occupied_loans_net_chargeoffs numeric(18, 0),
        item09_Non_farm_non_residential_other_loans numeric(18, 0),
        item09_Non_farm_non_residential_other_loans_net_chargeoffs numeric(18, 0),
        item10_Credit_cards numeric(18, 0),
        item10_Credit_cards_net_chargeoffs numeric(18, 0),
        item11_Automobile_loans numeric(18, 0),
        item11_Automobile_loans_net_chargeoffs numeric(18, 0),
        item12_Other_consumer_loans numeric(18, 0),
        item12_Other_consumer_loans_net_chargeoffs numeric(18, 0),
        item13_All_other_loans_and_leases_net_chargeoffs numeric(18, 0),
        item14_Total_loan_and_lease_net_charge_offs numeric(18, 0),
        item14_Loans_covered_by_FDIC_loss_sharing_agreements numeric(18, 0),
        item15_Total_loans_and_leases numeric(18, 0),
        item16_Allowance_for_loan_and_lease_losses numeric(18, 0),
        item17_US_government_obligations_and_obligations_of_GSEs_Securities numeric(18, 0),
        item18_Securities_issued_by_states_and_political_subdivisions_of_US_Securities numeric(18, 0),
        item19_Non_agency_MBS_and_ABS_securities_Securities numeric(18, 0),
        item20_All_other_HTM_securities numeric(18, 0),
        item21_Total_securities_HTM numeric(18, 0),
        item22_US_government_obligations_and_obligations_of_GSEs_Total_Securities numeric(18, 0),
        item23_Securities_issued_by_states_and_political_subdivisions_of_US_Total_Securities numeric(18, 0),
        item24_Non_agency_MBS_and_ABS_securities_Total_Securities numeric(18, 0),
        item25_All_other_AFS_securities numeric(18, 0),
        item26_Total_securities_AFS numeric(18, 0),
        item27_Trading_assets numeric(18, 0),
        item28_Total_intangible_assets numeric(18, 0),
        item29_Other_real_estate_owned numeric(18, 0),
        item30_All_other_assets numeric(18, 0),
        item31_Total_assets numeric(18, 0),
        item32_Retail_funding numeric(18, 0),
        item33_Wholesale_funding numeric(18, 0),
        item34_Trading_liabilities numeric(18, 0),
        item36_Total_liabilities numeric(18, 0),
        item37_Perpetual_preferred_stock_and_related_surplus numeric(18, 0),
        item38_Equity_capital numeric(18, 0),
        item39_Total_equity_capital numeric(18, 0),
        item40_Unrealized_gains_losses_on_AFS_securities numeric(18, 0),
        item41_Disallowed_deferred_tax_asset numeric(18, 0),
        item42_Tier_1_capital numeric(18, 0),
        item43_Qualifying_subordinated_debt_and_redeemable_preferred_stock numeric(18, 0),
        item44_Allowance_includible_in_Tier_2_capital numeric(18, 0),
        item45_Tier_2_capital numeric(18, 0),
        item_Total_risk_based_capital numeric(18, 0),
        item47_Total_capital numeric(18, 0),
        item48_Risk_weighted_assets numeric(18, 0),
        item49_Total_assets_for_leverage_purposes numeric(18, 0),
        item50_Tier_1_risk_based_capital_ratio numeric(18, 6),
        item51_Tier_1_leverage_ratio numeric(18, 6),
        item52_Total_risk_based_capital_ratio numeric(18, 6),
        item53_Sale_conversion_acquisition_or_retirement_of_capital_stock numeric(18, 0),
        item54_Cash_dividends_declared_on_preferred_stock numeric(18, 0),
        item55_Cash_dividends_declared_on_common_stock numeric(18, 0),
        item07_Dividends_share_repurchases_and_sale_conversion_acquisition_or_retirement_of_capital numeric(18, 0),
        item15_Net_interest_income numeric(18, 0),
        item16_Non_interest_income numeric(18, 0),
        item17_Non_interest_expense numeric(18, 0),
        item18_Pre_Provision_Net_Revenue numeric(18, 0),
        item19_Provision_for_loan_and_lease_losses numeric(18, 0),
        item20_Realized_gains_losses_on_HTM_securities numeric(18, 0),
        item21_Realized_gains_losses_on_AFS_securities numeric(18, 0),
        item23_Taxes numeric(18, 0),
        item24_Net_Income numeric(18, 0),
        item25_Total_other_than_temporary_impairment_OTTI_losses numeric(18, 0)
        )
      """
    return ddl

def DDL_MDRMCodes():
    ddl = """
        CREATE TABLE MDRMCodes(
            MDRM_Code nvarchar(15) NOT NULL,
            Short_Caption nvarchar(100) NOT NULL,
            Description nvarchar(500) NULL,
            Call_Report_Type nvarchar(15) NULL,
            Last_Update_Date datetime NULL
            )
      """
    return ddl