# -*- coding: utf-8 -*-

################################################################################
#
#      Connect to SQLServer on z9ped16
#
################################################################################


"""
Created on 2015.04.23

@author: pjc
@version: 0.1
"""


# standard imports for 2.x and 3.x compatibility
from __future__ import print_function
from __future__ import generators
from __future__ import division
from __future__ import with_statement
from __future__ import unicode_literals
from __future__ import nested_scopes
from __future__ import absolute_import

# odbc driver installed from:
#       https://code.google.com/p/pyodbc/downloads/list
import pyodbc


def sqlServerConnect():
    """
    hack: connects to z9ped16 server on pjc dev
    :return: sqlServer db connection
    """
    dbConn = pyodbc.connect(
        driver = 'SQL Server',
        host = 'Z9PED16\Z9PED16',
        database = 'FDIC Call Reports',
        user = 'pyodbc',
        password = 'password'
    )
    print(dbConn)
    return dbConn

def sqlServerShowTables(dbConn):
    cursor = dbConn.cursor()
    cursor.execute("""use "FDIC Call Reports";""")
    print(cursor)
    for row in cursor.tables(schema='dbo'):
        print(row)



def main():
    print('in main')
    dbConn = sqlServerConnect()
    cursor = dbConn.cursor()
    rows = cursor.execute('select top 10 * from [FDIC Call Reports].[dbo].[CroweCR_Branches]').fetchall()
    for row in rows:
        print(row)

    sqlServerShowTables(dbConn)



if __name__ == "__main__":
    main()
    #    __moduleInfo()
    #scriptPath = os.path.abspath(os.path.dirname(__file__))
    #print ("os.path.abspath(os.path.dirname(__file__)):", os.path.abspath(os.path.dirname(__file__)))
    #print ("os.path.dirname(__file__):", os.path.dirname(__file__))
    #print ("os.path.basename(__file__):", os.path.basename(__file__))
