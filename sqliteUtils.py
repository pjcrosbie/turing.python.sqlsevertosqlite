# -*- coding: utf-8 -*-

################################################################################
#
#      Connect to SQLite3 Database file
#
################################################################################


"""
Created on 2015.04.23

@author: pjc
@version: 0.1
"""


# standard imports for 2.x and 3.x compatibility
from __future__ import print_function
from __future__ import generators
from __future__ import division
from __future__ import with_statement
from __future__ import unicode_literals
from __future__ import nested_scopes
from __future__ import absolute_import

import sqlite3 as sqlite
import os
import sys
import itertools
import string
import decimal

def setCWDToFileDirectory():
    """
    Sets the python working directory to the folder this script was read from
    Useful for creating new databases in same folder as scripts
    :return: None
    """
    fullPathOfThisFile = os.path.abspath(os.path.dirname(__file__))
    os.chdir(fullPathOfThisFile)
    print("Current working dir: ", os.getcwd())

def sqliteConnect(dbFilename, existingOnly=False, verbose=True):
    """
    Connects to an existing database or creates a new one if it doesn't exist
    Prints name and version number of the database as a side effect
    :param dbFilename: the name of SQLite3 database file
    :param existingOnly: if true returns None if dbFilename does not exist
    :param verbose: true or false for print messages
    :return: a database connection object, or None
    """

    dbExists = os.path.isfile(dbFilename)
    if (existingOnly and not dbExists):
        # dbFilename doesn't exist and flagged must to connect
        # prevents accidental new db being created
        if (verbose):
            print('WARNING: connectToSQLite: sqlite connect failed, {0:} does not exist'.format(dbFilename))
        return None
    else:
        # the detect_types=sqlite3.PARSE_DECLTYPES is used to deal with column compatibility
        # see:
        # https://pysqlite.readthedocs.org/en/latest/sqlite3.html#using-adapters-to-store-additional-python-types-in-sqlite-databases
        dbConn = sqlite.connect(dbFilename, detect_types=sqlite.PARSE_DECLTYPES)

    if (verbose):
        # confirm file opened and print sqlite version
        cur = dbConn.cursor()
        cur.execute('SELECT SQLITE_VERSION()')
        ver = cur.fetchone()
        print ('INFO: connectToSQLite: SQLite file: {0:} version: {1:}'.format(dbFilename, ver[0]))
    # return a connection to db
    return dbConn

def tableExistsQ(dbConn, tableName, verbose=True):
    """
    Returns true if the table exists
    :param dbConn: the sqlite db connection
    :param tableName: we are checking for
    :param verbose: extra info
    :return: boolean, True if table exists, else False
    """
    cursor = dbConn.cursor()
    cursor.execute(""" SELECT COUNT(*) FROM sqlite_master WHERE name = ?  """, (tableName, ))
    res = cursor.fetchone()
    res = bool(res[0])
    if verbose:
        print("INFO: tableExistsQ: {0:} {1:}".format(tableName, res))
    return(res)

def createTable(dbConn, tableName, ddl, dropIfExists=True, verbose=True):
    """
    Creates a new table, tableName from the ddl
    :param dbConn: the sqlite db connection
    :param tableName: string
    :param ddl: string with full sqlite create table statement
    :param dropIfExists: boolean, drop it tableName exists, if false may fail
    :param verbose: extra info
    :return: None
    """
    cur = dbConn.cursor()
    if dropIfExists:
        sqlDropTable = 'drop table if exists {0:};'.format(tableName)
        resDropTable = cur.execute(sqlDropTable)
    # create table using ddl param
    resCreateTable = cur.execute(ddl)
    if verbose:
        print('INFO: sqliteCreateTable: drop table {0:}'.format(resDropTable))
        print('INFO: sqliteCreateTable: create table {0:} {1:}'.format(tableName, resCreateTable))

def columnCount(dbConn, tableName):
    """
    Returns the number of columns in tableName
    :param dbConn: sqlite connection
    :param tableName: string
    :return: int or None if tableName doesn't exist
    """
    tableExists = tableExistsQ(dbConn, tableName)
    if (not tableExists):
        return None

    cursor = dbConn.cursor()

    colCountQuery = 'PRAGMA table_info({0:})'.format(tableName)
    cursor.execute(colCountQuery)
    numberOfColumns = len(cursor.fetchall())

    return numberOfColumns

def rowCount(dbConn, tableName):
    """
    Returns the number of rows in tableName
    :param dbConn: sqlite connection
    :param tableName: string
    :return: int or None if tableName doesn't exist
    """
    tableExists = tableExistsQ(dbConn, tableName)
    if (not tableExists):
        return None

    cursor = dbConn.cursor()

    rowCountQuery = 'select count() from {}'.format(tableName)
    cursor.execute(rowCountQuery)
    numberOfRows = cursor.fetchone()[0]
    return numberOfRows

def flatten(nestedList):
    """
    returns the nestedList flattened, removing nested sublists
    handles nested lists with recursion (can't believe this isn't builtin
    [[1,2],3,[4,5,[6,7,[8]]]] => [1,2,3,4,5,6,7,8]
    :param alist: a nested list
    :return: flattened list
    """
    # trap these types to trigger recursion
    # there may be a smarter 'iterable' property way to do this but beyond me for now
    nestedTypes = (list, tuple)
    flattenedList = []
    for elem in nestedList:
        if isinstance(elem, nestedTypes):
            # recursion if nested elem
            flattenedList += flatten(elem)
        else:
            # just append basic elem
            flattenedList += [elem]

    return flattenedList

def riffle(alist, x ):
    """
    Returns a list with x interleaved into list
    riffle([1, 2, 3], x) => [1, x, 2, x, 3]
    :param list: any list
    :param x: a value
    :return: a list
    """
    res = []
    # loop over elem appending in interleaving fashion
    # loop not pythonic but only way I know to avoid sublists or flattening
    for i in range(len(alist)):
        res += [alist[i]]
        res += [x]

    # drop the last x from list, because add extra x at end
    resFinal = res[:-1]
    return resFinal

def makeInsertTemplate(columnCount):
    """
    Returns a string template for insert, i.e. (?, ?, ...?)
    :param columnCount: int
    :return: string
    """
    # core template of ?
    template = ['?'] * columnCount
    # riffle with commas
    res1 = string.join(template, ', ')
    # surround with ()
    finalRes = string.join(['(', res1, ')'])
    return finalRes

# adapt/convert for Decimal
# I don't understand why this builtin
def adapt_decimal(aDecimal):
    """
    converts a Decimal obj to a numeric for sqlite
    this actually loses some precision but is default affinity for Decimal in sqlite create table
    :param aDecimal: Decimal
    :return: a number
    """
    return float(aDecimal)

def convert_decimal(afloat):
    """
    converts a decimal column to float
    we leave as float as won't be pumping back to SQL Server and we aren't building an acctg system
    :param afloat:
    :return: float
    """
    return afloat

sqlite.register_adapter(decimal.Decimal, adapt_decimal)
# not sure why this needed to be encoded from unicode but leave for now, probably a 2.7 vs 3.x issue
sqlite.register_converter('Decimal'.encode('utf8'), convert_decimal)


def main():
    # set working dir to same dir as this file
    # setCWDToFileDirectory()
    # connect to existing db
    # dbConn = sqliteConnect('hse1.db', existingOnly=True)
    alist = ['a', 'b', 'c']
    print(alist)
    res = riffle(alist, ',')
    print(res)
    flatten(['a', 'b', 'c'])
    flatten([['a'], 'b', ['c', [1,2,[3,4]]] ])
    flatten([[1,2],3,[4,5,[6,7,[8]]]])
    makeInsertTemplate(5)

if __name__ == "__main__":
    main()


