# SQLServer to SQLite #

Simple scripts to extract data from SQL Sever and load SQLite table

main entry point is: dataPump.py

### WIP Initial Tests Only###

* Need to check on sqlite prepared statement for load step
* Data types are contributing to ballooning of the data file size
* Version 0.01

### 2015.04.12 Test Results ###

* 575MM rows, 8 columns
* 4 hours 50 mins to extract and load
* approx 35K records/sec
* sqlite file approx 50 GB