# -*- coding: utf-8 -*-

################################################################################
#
#      Pump data from SQLServer on z9ped16 to a SQLite3 database file
#
################################################################################


"""
Created on 2015.04.23

@author: pjc
@version: 0.1
"""


# standard imports for 2.x and 3.x compatibility
from __future__ import print_function
from __future__ import generators
from __future__ import division
from __future__ import with_statement
from __future__ import unicode_literals
from __future__ import nested_scopes
from __future__ import absolute_import

# odbc driver installed from:
#       https://code.google.com/p/pyodbc/downloads/list
import pyodbc

import sqlite3 as sqlite
import os
import sys
from decimal import *
import time

# see mylib
import Timer

import sqlServerUtils
import sqliteUtils
from CallReportDDL import *

def dataPumpTable(fromConn, fromTable, toConn, toTable, batchSize=25000, verbose=True, verboseRows=100000, overwrite=False):
    """
    pumps data from SQL Server fromTable to SQLite toTable
    assumes toTable already created and columns match exactly
    appends data by default, set overwrite=True to over write toTableName
    :param fromConn: a SQl Server connection
    :param fromTable: string, name of source SQL Server table
    :param toConn: a SQLite database connection
    :param toTable: string, name of destination SQLite table
    :param batchSize: int, size of pump stroke, cp batch rows at each transaction
    :param verbose: print extra info
    :param verboseRows, int report time every verboseRows
    :param overwrite: boolean, set True if you want to overwrite existing toTable
    :return: None
    """
    fromCursor = fromConn.cursor()
    toCursor = toConn.cursor()

    if overwrite:
        # then delete existing records in toTable
        if verbose:
            rowCount = sqliteUtils.rowCount(toConn, toTable)
            print('INFO: datapump: delete {} rows from {}'.format(rowCount, toTable))
        toCursor.execute('delete from {}'.format(toTable))
        toConn.commit()

    # pump
    fromCursor.execute('select * from {}'.format(fromTable))
    numberColumnsToInsert =sqliteUtils.columnCount(toConn, toTable)
    insertTemplate = sqliteUtils.makeInsertTemplate(numberColumnsToInsert)
    insertSQL = 'insert into {0:} values {1:}'.format(toTable, insertTemplate)
    done = False
    totRows = 0
    while not done:
        fromRows = fromCursor.fetchmany(batchSize)
        totRows += batchSize
        if (verbose and (totRows % verboseRows)):
            print('INFO: dataPumpTable: {} records processed {:,.0f}'.format(time.strftime('%X'),totRows))
        if len(fromRows) > 0:
            toCursor.executemany(insertSQL, fromRows)
            toConn.commit()
        else:
            done = True

def main():

    # get a connection to sqlServer - this is hack for pjc dev machine
    fromConn = sqlServerUtils.sqlServerConnect()

    # set working dir to same dir as this file
    sqliteUtils.setCWDToFileDirectory()

    # create new database, set existingOnly=False in prod
    toConn = sqliteUtils.sqliteConnect('hse1.db', existingOnly=False)

    # create tables
    sqliteUtils.createTable(toConn, "Branches", DDL_Branch())
    sqliteUtils.createTable(toConn, "CallReportData", DDL_CallReportData())
    sqliteUtils.createTable(toConn, "Demographics", DDL_Demographics())
    sqliteUtils.createTable(toConn, "FinancialStatement", DDL_FinancialStatement())
    sqliteUtils.createTable(toConn, "MDRMCodes", DDL_MDRMCodes())

    # example pump
    with Timer.Timer():
        dataPumpTable(fromConn, '[dbo].[CroweCR_CallReportData]', toConn, 'CallReportData', overwrite=True)
        rowsInserted = sqliteUtils.rowCount(toConn, 'CallReportData')
        print('Total rows inserted into {0:}: {1:,.0f}'.format('CallReportData', rowsInserted))


if __name__ == "__main__":
    main()
